(ns suffragium.runoff
  (:require [medley.core :refer [map-kv-vals remove-keys]]
            [suffragium.util :refer [lowkey before? into-sorted-array-map]]
            [suffragium.ballot-util :refer [top-ranked]]))

(defn- top-ranked-not-in
  "Return the top ranked candidate on ballot not in s"
  [ballot s]
  (top-ranked (apply dissoc ballot s)))

(defn- irv-result-comp
  "Internal function to sort IRV results correctly.

  [k1 v1] will place ahead [k2 v2] if v1 is longer (i.e. eliminated later).
  Ties are first broken by the final entry in v1/v2 (i.e. final vote count) and
  then by the order of k1 and k2 in the TBRC."
  [tbrc [k1 v1] [k2 v2]]
  (case (compare (count v1) (count v2)) ; no. of rounds
    1 true
    -1 false
    0 (case (compare (peek v1) (peek v2)) ; final vote count
        1 true
        -1 false
        0 (before? k1 k2 tbrc))))

(defn irv
  "Perform an election using instant runoff. Every ballot should contain a
  complete and strict ranking of the candidates. Returns an array map with
  candidate keys and values of vectors representing the number of votes
  that candidate had accumulated in each successive round. The map is ordered
  from most to least preferred candidate.

  :tbrc specifies the order ties should be broken in favour of when eliminating
  candidates. Should be a vector of all candidates from most preferred (to win
  a tie) to least preferred. Defaults to a complete, but non-specific, order.

  If :all-rounds is true (default: false) the runoff until all but one
  candidates have been eliminated. Otherwise the runoff will stop once a
  candidate reaches a majority."
  [ballots & {:keys [tbrc all-rounds] :or {all-rounds false}}]
  (let [tbrc (or tbrc (-> ballots first keys))
        majority (-> ballots count (quot 2) inc)
        empty-cand-map (zipmap tbrc (repeat []))]
    (loop [eliminated #{}
           piles (merge-with into empty-cand-map (group-by top-ranked ballots))
           results empty-cand-map]
      (let [tally (->> piles
                       (map-kv-vals #(count %2))
                       (remove-keys eliminated))
            dropped (->> tally lowkey (sort #(before? %1 %2 tbrc)) last)
            eliminated (conj eliminated dropped)
            results (merge-with conj results tally)]
        (if (or (= (count eliminated) (count tbrc))
                (and (not all-rounds)
                     (some (comp #(>= % majority) val) tally)))
          (into-sorted-array-map (partial irv-result-comp tbrc) results)
          (recur eliminated
                 (merge-with into
                             (group-by #(top-ranked-not-in % eliminated)
                                       (dropped piles))
                             (dissoc piles dropped)) ; redistribute votes
                 results))))))

