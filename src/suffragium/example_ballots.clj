(ns suffragium.example-ballots)

;; https://youtu.be/HoAnYQZrNrQ
;; https://www.ams.org/publicoutreach/feature-column/fcarc-voting-decision
(def pbs (concat (repeat 18 {:green 1 :blue 5 :purple 4 :red 2 :orange 3})
                 (repeat 12 {:green 5 :blue 1 :purple 4 :red 3 :orange 2})
                 (repeat 10 {:green 5 :blue 2 :purple 1 :red 4 :orange 3})
                 (repeat  9 {:green 5 :blue 4 :purple 2 :red 1 :orange 3})
                 (repeat  4 {:green 5 :blue 2 :purple 4 :red 3 :orange 1})
                 (repeat  2 {:green 5 :blue 4 :purple 2 :red 3 :orange 1})))

(def tennessee (concat (repeat 42 {:Memphis 1 :Nashville 2 :Chattanooga 3 :Knoxville 4})
                       (repeat 26 {:Memphis 4 :Nashville 1 :Chattanooga 2 :Knoxville 3})
                       (repeat 17 {:Memphis 4 :Nashville 3 :Chattanooga 2 :Knoxville 1})
                       (repeat 15 {:Memphis 4 :Nashville 3 :Chattanooga 1 :Knoxville 2})))

;; adapted from https://en.wikipedia.org/wiki/Copeland%27s_method
(def paradox (concat (repeat 31 {:A 1 :B 5 :C 3 :D 4 :E 2})
                     (repeat 30 {:A 2 :B 1 :C 4 :D 4 :E 3})
                     (repeat 29 {:A 4 :B 3 :C 1 :D 2 :E 4})
                     (repeat 10 {:A 2 :B 4 :C 4 :D 1 :E 3})))

;; https://electowiki.org/wiki/Beatpath_example_12
(def beatpath12 (concat (repeat  6 {:A  1 :B  2 :C  3 :D  3 :E  3 :F  3 :G  3 :H  3 :I  3 :J  3 :K  3 :L  3})
                        (repeat  1 {:A  1 :B  5 :C  2 :D  4 :E  8 :F  7 :G  6 :H  3 :I 10 :K 11 :J 12 :L  9})
                        (repeat  6 {:A  3 :B  3 :C  1 :D  2 :E  3 :F  3 :G  3 :H  3 :I  3 :K  3 :J  3 :L  3})
                        (repeat  3 {:A  2 :B  6 :C  3 :D  5 :E  1 :F  7 :G  7 :H  4 :I  7 :K  7 :J  7 :L  7})
                        (repeat  3 {:A  5 :B  2 :C  4 :D  3 :E  1 :F  6 :G  6 :H  6 :I  6 :K  6 :J  6 :L  6})
                        (repeat  2 {:A  5 :B  6 :C  7 :D  7 :E  1 :F  2 :G  3 :H  4 :I  7 :K  7 :J  7 :L  7})
                        (repeat  2 {:A  7 :B  5 :C  6 :D  7 :E  1 :F  3 :G  2 :H  4 :I  7 :K  7 :J  7 :L  7})
                        (repeat  4 {:A  5 :B  6 :C  6 :D  2 :E  6 :F  1 :G  3 :H  4 :I  6 :K  6 :J  6 :L  6})
                        (repeat  1 {:A  8 :B  5 :C  7 :D  6 :E  3 :F  1 :G  2 :H  4 :I 10 :K 10 :J 10 :L  9})
                        (repeat  4 {:A  6 :B  2 :C  5 :D  6 :E  6 :F  3 :G  1 :H  4 :I  6 :K  6 :J  6 :L  6})
                        (repeat  1 {:A 12 :B  7 :C  6 :D  5 :E  4 :F  3 :G  2 :H  1 :I  9 :K 10 :J  8 :L 11})
                        (repeat  9 {:A 12 :B  7 :C  6 :D  5 :E  4 :F  3 :G  2 :H  1 :I  9 :K 11 :J 10 :L  8})
                        (repeat 10 {:A  5 :B  6 :C  7 :D  8 :E  9 :F 10 :G 11 :H 12 :I  3 :K  1 :J  2 :L  4})))

(def dinner (concat (repeat 2 {:Steak true  :Burger true :Veggie false})
                    (repeat 1 {:Steak false :Burger true :Veggie false})
                    (repeat 3 {:Steak false :Burger true :Veggie true})))

(def movie '({:terminator2 true  :spirited-away true  :wall-e true}
             {:terminator2 false :spirited-away true  :wall-e true}
             {:terminator2 true  :spirited-away false :wall-e false}
             {:terminator2 false :spirited-away true  :wall-e true}
             {:terminator2 true  :spirited-away false :wall-e true}))
