(ns suffragium.loom-util
  (:require [loom.graph :refer [edges weight successors]]
            [loom.alg :refer [topsort]]))

(defmulti rev-edge
  "Reverses the direction of a graph edge in the form [src dest] or
  [src dest weight]. Does not change weight."
  count)
(defmethod rev-edge 2
  [[src dest]]
  [dest src])
(defmethod rev-edge 3
  [[src dest w]]
  [dest src w])

(defn wedges
  "Edges in weighted graph wdg including weights. May return each edge twice in
  an undirected graph."
  [wdg]
  (map #(conj % (weight wdg %)) (edges wdg)))

(defn cycles?
  [g]
  (nil? (topsort g)))

; https://github.com/aysylu/loom/pull/111
(defn simple-paths
  "Finds all simple paths from start node to end node. Paths are represented as
  a collection of nodes in traversal order."
  [g start end & {:keys [max-depth] :or {max-depth nil}}]
  (if (= start end)
    [[]]
    (letfn [(create-path-map []
              {:members #{}
               :path []})
            (add-path-node [pm n]
              (-> pm
                  (update :members conj n)
                  (update :path conj n)))]
      (loop [q #?(:clj clojure.lang.PersistentQueue/EMPTY
                  :cljs cljs.core/PersistentQueue.EMPTY)
             completed-paths []
             p (-> (create-path-map)
                   (add-path-node start))]
        (let [p-last (-> p :path peek)
              unseen-succs (filter (comp not (:members p)) (successors g p-last))
              succ-ps (map (partial add-path-node p) unseen-succs)
              updated-q (reduce conj q (filter (fn [succ-p]
                                                 ;; Check if not a completed path and not
                                                 ;; longer than max-depth, if specified
                                                 (and (-> succ-p :path peek (= end) not)
                                                      (if (nil? max-depth)
                                                        true
                                                        (-> succ-p :path count (< max-depth)))))
                                               succ-ps))
              updated-completed-paths (reduce conj
                                              completed-paths
                                              (->> succ-ps
                                                   (filter (comp (partial = end) peek :path))
                                                   (map :path)))]
          (if (-> updated-q seq)
            (recur (pop updated-q)
                   updated-completed-paths
                   (peek updated-q))
            updated-completed-paths))))))
