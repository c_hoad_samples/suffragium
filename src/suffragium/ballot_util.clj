(ns suffragium.ballot-util
  (:require [medley.core :refer [map-vals]]
            [suffragium.util :refer [lowkey]]))

(defn top-ranked
  "Returns the most-preferred candidate on the ballot. If there are multiple
  such candidates one is chosen in an undefined way."
  [ballot]
  (when (seq ballot) (-> ballot lowkey first)))

(defn map-ballot
  "Replace the value associated with each candidate in a ballot with the result
  of applying f to it.

  Useful for e.g. converting ranked ballot to a ballot suitable for approval
  voting."
  [f ballot]
  (map-vals f ballot))

(defn pairwise-preferred
  "Given two candidates in cands, returns the preferred of those candidates in
  ballot. Returns nil if neither is preferred over the other."
  [ballot cands]
  (let [b (select-keys ballot cands)]
    (case (compare (-> b first val) (-> b second val))
      -1 (-> b first key)
      0 nil
      1 (-> b second key))))
