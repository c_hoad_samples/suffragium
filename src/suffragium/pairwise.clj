(ns suffragium.pairwise
  (:require [medley.core :refer [find-first map-kv-vals]]
            [loom.graph :refer [weighted-digraph predecessors in-degree weight
                                out-degree directed? weighted? nodes add-edges
                                digraph remove-edges]]
            [loom.alg :refer [scc topsort]]
            [loom.derived :refer [nodes-filtered-by]]
            [net.cgrand.xforms :as xf]
            [clojure.set :refer [union subset?]]
            [clojure.math.combinatorics :refer [combinations
                                                permuted-combinations]]
            [suffragium.util :refer [unwrap-set rvec index-of before?
                                     into-sorted-array-map]]
            [suffragium.ballot-util :refer [pairwise-preferred]]
            [suffragium.loom-util :refer [rev-edge wedges cycles?
                                          simple-paths]]))

(def ^:private score-result-comp #'suffragium.util/score-result-comp)

(defn- make-pairwise-tally
  "Takes a lit of candidates and creates the structure used to store pairwise
  election results."
  [cand]
  (zipmap
   (map set (combinations cand 2))
   (repeat {:winner ::tie :margin 0})))

(defn- pmargin
  [me]
  (-> me val :margin))

(defn- pwinner
  [me]
  (-> me val :winner))

(defn- ploser
  [me]
  (let [w (pwinner me)]
    (if (= w ::tie)
      ::tie
      (unwrap-set (disj (key me) w)))))

;; to turn a pairwise election into a graph edge would usually be easy, just
;; [winner loser margin]. only wrinkle is ties. everything works out neatly(ish)
;; if they are represented as a weight 0 edge from a to b and another from b
;; to a. so this transformer spits out [[winner loser margin]] if there isn't
;; a tie and [[a b 0] [b a 0]] if there is
;; pairwise-edges does the dirty work of conj'ing this all together
(def ^{:private true} pairwise-edges-xf
  (map
   #(let [w (pwinner %)]
      (if (= ::tie w)
        (let [cands (vec (key %))]
          [(conj cands 0) (-> cands rvec (conj 0))])
        [[w (ploser %) (pmargin %)]]))))

(defn- pairwise-edges
  "Takes a pairwise election result and returns a vector of the edges in the
  corresponding directed graph in a form usable by loom's weighted-digraph
  constructor. In the case of ties two edges are listed between the
  corresponding candidates in either direction and weight 0."
  [p]
  (transduce pairwise-edges-xf into p))

(defn- p->wdg
  "Converts a pairwise election result to a loom weighted digraph."
  [p]
  (apply weighted-digraph (pairwise-edges p)))

(defn- inc-pairwise
  "Increment a pairwise election result. Increments the margin if pref is the
  winning candidate, else decrements the margin. Sets the winner to ::tie if
  the margin becomes 0. If the election is currently tied, sets the winner to
  pref."
  [t pref]
  (if (= ::tie (:winner t))
    (-> t (assoc :winner pref) (update :margin inc))
    (let [t (update t :margin (if (= (:winner t) pref) inc dec))]
      (if (zero? (:margin t)) (assoc t :winner ::tie) t))))

(defn- pairwise-tally
  "Update the tally to reflect the pairwise preferences expressed by ballot."
  [p ballot]
  (map-kv-vals (fn [k v] (if-let [pref (pairwise-preferred ballot k)]
                           (inc-pairwise v pref) v))
               p))

(defn- pairwise-impl
  "Perform a pairwise election. Returns a map of candidate pair sets to results
  of their pairwise elections (a map with a :winner key giving the winning
  candidate and a :margin key giving the margin of victory, the winner of a tied
  election is ::tie and has a margin of 0).

  This structure is only used internally and is used to build a weighted
  directed graph with loom (see p->wdg)."
  [ballots]
  (reduce
   pairwise-tally
   (-> ballots first keys make-pairwise-tally)
   ballots))

(defn pairwise
  "Perform a pairwise election. Returns a loom weighted digraph. A pairwise
  election between two candidates is represented as a directed edge from the
  winner to the loser with a weight of the margin of victory. In the case of
  an edge to and from both candidates is created with a weight of 0.

  Each ballot should contain a key for each candidate with an integer, lower
  value = more preferred. Candidates may be ranked as equally preferable."
  [ballots]
  (if (= (-> ballots first count) 1)
    (weighted-digraph (-> ballots ffirst key))  ; 1 candidate -> lone node
    (-> ballots pairwise-impl p->wdg)))

(defn invert
  "Takes the result of a pairwise election and reverses the direction of every
  edge. Useful if you want to find e.g. the condorcet loser."
  [pg]
  (apply weighted-digraph (map rev-edge (wedges pg))))

;; used when beatpaths rather than beat-or-tie paths must be considered
;; we would like to use edges-filtered-by for this but that seems to always
;; return an unweighted digraph
(defn- drop-tied-edges
  [pg]
  (transduce
   (comp (filter #(-> % peek zero?)) (map pop))
   (completing remove-edges)
   pg
   (wedges pg)))


;; the condorcet winner wins all pairwise elections, ergo it has no
;; predecessors in a directed graph so find that node if it exists


(defn- condorcet-impl
  [pg]
  (->> pg
       (nodes-filtered-by (comp zero? (partial in-degree pg)))
       nodes))

(defmulti condorcet
  "Returns the condorcet winner if one exists from either a seq of ballots or
  the result of a pairwise election. In the former case a pairwise election
  is performed. Returns nil if there is no condorcet winner."
  (every-pred weighted? directed?))
(defmethod condorcet true
  [pg]
  (-> pg condorcet-impl unwrap-set))
(defmethod condorcet false
  [ballots]
  (-> ballots pairwise condorcet))

(defmulti weak-condorcet
  "Returns the weak condorcet winners if any exist from either a seq of ballots
  or the result of a pairwise election. In the former case a pairwise election
  is performed. Returns #{} if there are no weak condorcet winners."
  (every-pred weighted? directed?))
(defmethod weak-condorcet true
  [pg]
  (-> pg drop-tied-edges condorcet-impl))

(defmethod weak-condorcet false
  [ballots]
  (-> ballots pairwise weak-condorcet))

(defn- predecessors-of
  "Return the predecessors of multiple nodes"
  [g nodes]
  (transduce (map (partial predecessors g)) union nodes))

;; the smith set is one of the strongly connected cycles in the graph,
;; specifically the scc with no predecessors outside it
;; smith set cannot be empty so if there is one cycle it must be the smith set
(defn smith-set
  "Takes the results of a pairwise election and returns the Smith set."
  [pg]
  (set
   (let [cs (scc pg)]
     (if (= (count cs) 1)
       (first cs)
       (find-first #(subset? (predecessors-of pg %) (set %)) cs)))))

(defn schwartz-components
  "Takes the results of a pairwise election and returns the Schwartz set
  components in no particular order."
  [pg]
  (map set
       (let [pg (drop-tied-edges pg)
             cs (scc pg)]
         (if (= (count cs) 1)
           cs
           (filter #(subset? (predecessors-of pg %) (set %)) cs)))))

(defn schwartz-set
  "Takes the results of a pairwise election and returns the Schwartz set."
  [pg]
  (apply union (schwartz-components pg)))

(defmulti copeland
  "Perform a Copeland method election. Returns an array map of candidate keys
  and score values ordered from greatest to smallest score. The score of a
  candidate is its number of pairwise victories minus its number of pairwise
  defeats."
  (fn [x & _] ((every-pred directed?) x)))
(defmethod copeland true
  [pg & {:keys [tbrc]}]
  (let [tbrc (or tbrc (-> pg nodes vec))]
    (into-sorted-array-map
     (partial score-result-comp tbrc)
     (zipmap tbrc (map #(- (out-degree pg %) (in-degree pg %)) tbrc)))))
(defmethod copeland false
  [ballots & {:keys [tbrc]}]
  (copeland (pairwise ballots) :tbrc tbrc))

(defmulti llull-copeland
  "Perform a Copeland method election as originally proposed by Llull. Returns
  an array map of candidate keys and score values ordered from greatest to
  smallest score. The score of a candidate is its number of pairwise victories."
  (fn [x & _] ((every-pred directed?) x)))
(defmethod llull-copeland true
  [pg & {:keys [tbrc]}]
  (let [pg (drop-tied-edges pg)
        tbrc (or tbrc (-> pg nodes vec))]
    (into-sorted-array-map
     (partial score-result-comp tbrc)
     (zipmap tbrc (map (partial out-degree pg) tbrc)))))
(defmethod llull-copeland false
  [ballots & {:keys [tbrc]}]
  (llull-copeland (pairwise ballots) :tbrc tbrc))

;; http://lists.electorama.com/pipermail/election-methods-electorama.com/2004-May/078350.html
(defn- zavist
  [rank [i j] [m n]]
  (letfn [(tbrc [c] (index-of rank c))]
    (or (< (min (tbrc i) (tbrc j)) (min (tbrc m) (tbrc n)))
        (and (= (min (tbrc i) (tbrc j)) (min (tbrc m) (tbrc n)))
             (< (max (tbrc i) (tbrc j)) (max (tbrc m) (tbrc n)))))))
(defn- cretney
  [rank [i j] [m n]]
  (or (before? i m rank)
      (and (= i m) (before? n j rank))))

(defn- legrand
  [rank [i j] [m n]]
  (or (before? n j rank)
      (and (= j n) (before? i m rank))))

(defn- schulze-i
  [rank [i j] [m n]]
  (or (and (before? i j rank) (before? n m rank))
      (and (before? i j rank) (before? m n rank) (before? i m rank))
      (and (before? j i rank) (before? n m rank) (before? i m rank))
      (and (= i m) (before? n j rank))
      (and (= j n) (before? i m rank))))

(defn- schulze-ii
  [rank [i j] [m n]]
  (or (and (before? i j rank) (before? n m rank))
      (and (before? i j rank) (before? m n rank) (before? n j rank))
      (and (before? j i rank) (before? n m rank) (before? n j rank))
      (and (= i m) (before? n j rank))
      (and (= j n) (before? i m rank))))

(defn- get-tbrp
  [x tbrc]
  (if (vector? x)
    #(before? %1 %2 x)
    (case x
      :zavist (partial zavist tbrc)
      :cretney (partial cretney tbrc)
      :legrand (partial legrand tbrc)
      :schulze-i (partial schulze-i tbrc)
      :schulze-ii (partial schulze-ii tbrc))))

(defn- rp-comp
  [tiebreak [x1 y1 w1] [x2 y2 w2]]
  (case (compare w1 w2)
    1 true
    -1 false
    0 (tiebreak [x1 y1] [x2 y2])))

(defmulti ranked-pairs
  "Perform an election using Tideman's ranked pairs method. Returns an array
  map of candidate keys and score values, ordered from greatest to smallest
  score. The score of a candidate is its Copeland score on the graph of
  locked-in candidate pairs.

  Accepts two optional arguments:

  :tbrc is a tie-breaking order of the candidates, used to construct a
  tie-breaking ranking of the pairs. Used when pairs have the same margin.
  Must be a vector containing all candidates from most-preferred to least
  preferred. The default order should be considered an implementation detail.

  :tbrp specifies the method used to construct a TBRP from the TBRC. Should be
  one of :zavist, :cretney, :legrand, :schulze-i, or :schulze-ii. Alternatively
  can be a vector containing all pairs of candidates as sub-vectors (in which
  case :tbrc is ignored). Defaults to :schulze-ii."
  (fn [x & _] ((every-pred weighted? directed?) x)))
(defmethod ranked-pairs false
  [ballots & {:keys [tbrc tbrp]
              :or {tbrp :schulze-ii}}]
  (ranked-pairs (pairwise ballots) :tbrc tbrc :tbrp tbrp))
(defmethod ranked-pairs true
  [pg & {:keys [tbrc tbrp]
         :or {tbrp :schulze-ii}}]
  (let [tbrc (or tbrc (-> pg nodes sort vec))
        tbrp-comp (get-tbrp tbrp tbrc)]
    (transduce
     (xf/sort (partial rp-comp tbrp-comp))
     (completing
      (fn [g edge]
        (let [g+ (add-edges g (vec edge))]
          (if (cycles? g+) g g+)))
      copeland)
     (digraph)
     (wedges pg))))

(defn- strongest-weakest-link
  [g source sink]
  (let [paths (simple-paths g source sink)]
    (when (seq paths)
      (vec (reduce
            (partial max-key #(apply weight g %))
            (map (comp
                  (partial apply min-key #(apply weight g %))
                  (partial partition 2 1))
                 paths))))))

;; TODO: memoize strongest-weakest-link
(defn- schulze-comp
  [g tiebreak [a b] [x y]]
  (let [nweight (fn [g e] (if (nil? e) 0 (weight g e)))
        swlab (strongest-weakest-link g a b)
        swlxy (strongest-weakest-link g x y)]
    (case (compare (nweight g swlab) (nweight g swlxy))
      1 true
      -1 false
      0
      (let [ab<xy (tiebreak [a b] [x y])]
        (if (= nil swlab swlxy)  ;; no paths left
          ab<xy
          (schulze-comp (remove-edges g (if ab<xy swlxy swlab)) tiebreak [a b] [x y]))))))

(defmulti schulze
  "Perform an election using the Schulze method method. Returns a sequence of
  candidates from highest to lowest ranking. Uses margins to measure the
  strength of links.

  Accepts two optional arguments:

  :tbrc is a tie-breaking order of the candidates, used to construct a
  tie-breaking ranking of the pairs. Used when pairs have the same strength
  beatpath. Must be a vector containing all candidates from most-preferred to
  least preferred. If not specified the TBRC is the order of candidates
  returned by sort.

  :tbrp specifies the method used to construct a TBRP from the TBRC. Should be
  one of :zavist, :cretney, :legrand, :schulze-i, or :schulze-ii. Alternatively
  can be a vector containing all pairs of candidates as sub-vectors (in which
  case :tbrc is ignored). Defaults to :schulze-ii."
  (fn [x & _] ((every-pred weighted? directed?) x)))
(defmethod schulze false
  [ballots & {:keys [tbrc tbrp]
              :or {tbrp :schulze-ii}}]
  (schulze (pairwise ballots) :tbrc tbrc :tbrp tbrp))
(defmethod schulze true
  [pg & {:keys [tbrc tbrp]
         :or {tbrp :schulze-ii}}]
  (let [pg (drop-tied-edges pg)
        tbrc (or tbrc (-> pg nodes sort vec))
        tbrp-comp (get-tbrp tbrp tbrc)]
    (topsort
     (reduce
      (fn [g edge]
        (let [g+ (add-edges g (vec edge))]
          (if (cycles? g+) g g+)))
      (digraph)
      (sort (partial schulze-comp pg tbrp-comp)
            (permuted-combinations (nodes pg) 2))))))
