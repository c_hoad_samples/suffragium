(ns suffragium.cardinal-bench
  (:require [libra.bench :refer :all]
            [libra.criterium :as c]
            [suffragium.cardinal :refer :all]
            [suffragium.example-ballots :refer :all]))

(defbench borda-count-beatpath12
  (is (c/bench (borda-count beatpath12))))

(defbench plurality-beatpath12
  (is (c/bench (plurality beatpath12))))
