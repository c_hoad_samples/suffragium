# suffragium

[![Clojars](https://img.shields.io/clojars/v/suffragium.svg)](https://clojars.org/suffragium)

A Clojure library for counting votes

## Features

Currently supported voting methods:

+ Plurality
+ Approval
+ Borda count (score)
+ Instant runoff
+ STAR
+ Condorcet
+ Copeland
+ Ranked pairs
+ Schulze
+ Single stochastic vote

## Documentation

[API Docs on cljdoc](https://cljdoc.org/d/suffragium/suffragium/)

## Disclaimer

This code is pre-alpha. I have no formal training in social choice theory. The
implementations of the voting methods in this library have not been audited. As
such this library is not in a fit state for use in any non-academic contexts.
Using this library to count votes in an election in your
organisation/company/state may result in side-effects including but not limited
to:

+ Incorrect results
+ Correct results
+ Crop failure
+ Raiders-of-the-Lost-Ark-style face melting
+ Dogs and cats living together
+ Mass hysteria

## License

EPL 2.0 or later

Copyright © 2020 Corin Hoad
